目标，做最小的 maven + spring3 + hibernate4 + struts2
适合人群：初学者。。
项目说明 ：使用maven构建的spring3 + hibernate4 + struts2,hibernate数据源使用阿里巴巴的druid数据源。
运行此项目之前，请确保安装以下程序：
1.jdk
2.tomcat
3.maven
4.myeclipse/intellij idea
5.mysql

此项目使用mysql数据库，运行之前，请先创建数据库和表。

数据库：workstudy;
表：work;

sql语句：
create database workstudy;
create table work(workID int primary key auto_increment,category varchar(30), name varchar(50),total int);


包说明 ：
action：com.taoxue.minmssh.action 
dao接口：com.taoxue.minmssh.dao
dao实现：com.taoxue.minmssh.dao.impl
持久化数据：com.taoxue.minmssh.model
service接口：com.taoxue.minmssh.service
service实现：com.taoxue.minmssh.service.impl


配置文件统一放在 src/main/resources目录下










