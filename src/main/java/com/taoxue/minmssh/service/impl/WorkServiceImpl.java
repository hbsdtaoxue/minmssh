package com.taoxue.minmssh.service.impl;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.taoxue.minmssh.dao.IWorkDao;
import com.taoxue.minmssh.model.Twork;
import com.taoxue.minmssh.service.IWorkService;

@Service("workService")
public class WorkServiceImpl implements IWorkService{
	private IWorkDao workDao;
	
	public IWorkDao getWorkdao() {
		return workDao;
	}

	@Resource(name="workDao")
	public void setWorkdao(IWorkDao workdao) {
		this.workDao = workdao;
	}

	@Override
	public void save(Twork work) {
		workDao.save(work);
	}
}
