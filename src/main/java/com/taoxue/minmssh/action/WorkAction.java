package com.taoxue.minmssh.action;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.taoxue.minmssh.model.Twork;
import com.taoxue.minmssh.service.IWorkService;

/**
 * struts中的action。也可以使用注释，不用在struts.xml文件 中配置。
 * @author Administrator
 *
 */
public class WorkAction extends ActionSupport {
	private IWorkService workService;

	public IWorkService getWorkService() {
		return workService;
	}

	@Resource(name = "workService")
	public void setWorkService(IWorkService workService) {
		this.workService = workService;
	}

	public String save() {
		Twork work = new Twork();
		work.setCategory("家教");
		work.setName("小学六年级英语");
		work.setTotal(10);
		workService.save(work);
		return "success";
	}

}
