package com.taoxue.minmssh.dao.impl;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.taoxue.minmssh.dao.IWorkDao;
import com.taoxue.minmssh.model.Twork;

@Repository("workDao")
public class WorkDaoImpl implements IWorkDao {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Resource(name = "sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void save(Twork work) {
		this.getSessionFactory().getCurrentSession().save(work);
	}
}
