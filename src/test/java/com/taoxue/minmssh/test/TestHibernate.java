package com.taoxue.minmssh.test;

import java.util.Date;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.taoxue.minmssh.model.Twork;
import com.taoxue.minmssh.service.IWorkService;

public class TestHibernate {

	@Test
	public void test() {
		ApplicationContext ac = new ClassPathXmlApplicationContext(
				new String[] { "classpath:spring.xml",
						"classpath:spring-hibernate.xml" });
		IWorkService workService = (IWorkService) ac.getBean("workService");
		Twork work = new Twork();

		work.setCategory("家教");
		work.setName("小学六年级语文");
		work.setTotal(10);
		workService.save(work);
	}

}
